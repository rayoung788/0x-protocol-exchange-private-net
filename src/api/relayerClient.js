const { HttpClient } = require('@0xproject/connect')
const { ZeroEx } = require('0x.js')
const { BigNumber } = require('@0xproject/utils')
// client sued to interface with the relay server
class RelayerClient {
  constructor(zeroEx, base, quote) {
    // the current zeroEx instance
    this.zeroEx = zeroEx
    this.requestId = 0
    // the current orderbook state
    this.orderbook = { bids: {}, asks: {} }
    // the current BASE/QUOTE token pairs
    this.baseToken = {
      address: '',
      symbol: base
    }
    this.quoteToken = {
      address: '',
      symbol: quote
    }
    // variable to keep track of the websocket startup status
    this.loading = {start: true}
    // address of the WETH contract (for conversions ETH <-> WETH)
    this.weth = ''
    // address of the exchange contract (for order fills)
    this.exchange = ''
    // the websocket instance
    this.ws = {}
    // the relay server connector insatnce
    this.rest = new HttpClient(`http://${window.location.host}/v0`) // http://localhost:3000 `http://${window.location.host}/v0`
    // Initiates the websocket startup
    this.start()
  }

  start() {
    // create the new websocket
    this.ws = new WebSocket(`ws://${window.location.host.replace('03', '04')}/v0`) // ws://localhost:3004/v0 ws://${window.location.host.replace('03', '04')}/v0
    // specify the opening message
    this.ws.addEventListener('open', event => {
      this.open()
    })
    // specify handling for normal message updates
    this.ws.addEventListener('message', event => {
      this.parse(JSON.parse(event.data))
    })
  }

  async open(event) {
    // Get the token info for WETH from the token registry contract
    const wethTokenInfo = await this.zeroEx.tokenRegistry.getTokenBySymbolIfExistsAsync('WETH')
    // Get the token info of the BASE/QUOTE pair from the token registry contract
    const quoteTokenInfo = await this.zeroEx.tokenRegistry.getTokenBySymbolIfExistsAsync(this.quoteToken.symbol)
    const baseTokenInfo = await this.zeroEx.tokenRegistry.getTokenBySymbolIfExistsAsync(this.baseToken.symbol)
    // Get the exchange contract address for order fills
    this.exchange = await zeroEx.exchange.getContractAddress()
    // Check if either getTokenBySymbolIfExistsAsync query resulted in undefined
    if (baseTokenInfo === undefined || quoteTokenInfo === undefined) {
      throw new Error('could not find token info')
    }
    // update local BASE/QUOTE state using returned registry information
    this.baseToken = baseTokenInfo
    this.quoteToken = quoteTokenInfo
    // update local WETH state using returned registry information
    this.weth = wethTokenInfo.address
    // Opening websocket sequence to request order updates for the specified BASE/QUOTE pair
    // Also requests a snapshot of the relay's orderbook to start from
    this.ws.send(
      JSON.stringify({
        type: 'subscribe',
        channel: 'orderbook',
        requestId: this.requestId,
        payload: {
          baseTokenAddress: baseTokenInfo.address,
          quoteTokenAddress: quoteTokenInfo.address,
          snapshot: true,
          limit: 20
        }
      })
    )
    this.requestId += 1
    console.log(`Listening for ${this.baseToken.symbol}/${this.quoteToken.symbol} orderbook...`)
    // declare the startup process complete
    this.loading.start = false
  }
  parse(m) {
    // logic to handle different websocket updates
    console.log(m)
    if (m.type === 'snapshot') {
      // Snapshots: replace the local snapshot with the incoming snapshot
      this.orderbook.bids = m.payload.bids
      this.orderbook.asks = m.payload.asks
    }
    if (m.type === 'update') {
      // New Order: add the new order to the local orderbook
      if (m.payload.makerTokenAddress === this.baseToken.address && m.payload.takerTokenAddress === this.quoteToken.address) {
        // asks
        const asks = {...this.orderbook.asks}
        asks[m.payload.idx] = m.payload
        this.orderbook.asks = asks
      } else if (m.payload.makerTokenAddress === this.quoteToken.address && m.payload.takerTokenAddress === this.baseToken.address) {
        // bids
        const bids = {...this.orderbook.bids}
        bids[m.payload.idx] = m.payload
        this.orderbook.bids = bids
      }
    }
    if (m.type === 'prune') {
      // Invalid Order: remove the invalid order to the local orderbook
      if (m.payload.makerTokenAddress === this.baseToken.address && m.payload.takerTokenAddress === this.quoteToken.address) {
        // asks
        const asks = {...this.orderbook.asks}
        delete asks[m.payload.idx]
        this.orderbook.asks = asks
      } else if (m.payload.makerTokenAddress === this.quoteToken.address && m.payload.takerTokenAddress === this.baseToken.address) {
        // bids
        const bids = {...this.orderbook.bids}
        delete bids[m.payload.idx]
        this.orderbook.bids = bids
      }
    }
  }
  async newOrder(address, makerAddress, takerAddress, makerAmount, takerAmount) {
    // creates a new order and sends it to the relay server
    const relayerClient = this.rest
    const decimals = this.quoteToken.decimals
    // all new orders generated using this method are hard-coded to last for one day
    // TODO: make this configurable
    const ONE_DAY_IN_MS = 1000 * 60 * 60 * 24
    // Generate fees request for the order
    const feesRequest = {
      exchangeContractAddress: this.exchange,
      maker: address,
      // sets the taker address to null so that anyone can fill the order
      taker: ZeroEx.NULL_ADDRESS,
      makerTokenAddress: makerAddress,
      takerTokenAddress: takerAddress,
      // ZeroEx expects time and amounts to be big numbers
      makerTokenAmount: ZeroEx.toBaseUnitAmount(new BigNumber(makerAmount), decimals),
      takerTokenAmount: ZeroEx.toBaseUnitAmount(new BigNumber(takerAmount), decimals),
      expirationUnixTimestampSec: new BigNumber(Date.now() + ONE_DAY_IN_MS),
      salt: ZeroEx.generatePseudoRandomSalt()
    }
    // Send fees request to relayer and receive a FeesResponse instance
    const feesResponse = await relayerClient.getFeesAsync(feesRequest)
    // Combine the fees request and response to from a complete order
    const order = {
      ...feesRequest,
      ...feesResponse
    }
    // Create orderHash
    const orderHash = ZeroEx.getOrderHashHex(order)
    // Sign orderHash and produce a ecSignature
    const ecSignature = await this.zeroEx.signOrderHashAsync(orderHash, address, true)
    // Append signature to order
    const signedOrder = {
      ...order,
      ecSignature
    }
    // Submit order to relayer
    await relayerClient.submitOrderAsync(signedOrder)
  }
}

export default RelayerClient
