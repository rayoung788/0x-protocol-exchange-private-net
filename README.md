# 0x Protocol Private Network Exchange

> User interface and relay server for the 0x Protocol contracts hosted on the Cuvia Labs Private Ethereum Network. 
> 
> Currently supports the following functionality: viewing asks & bids, filling orders, creating new orders, converting ETH <-> WETH, and setting token allowances. 
> 
> Includes support for a number of test tokens and the CUV token for demo purposes.
> 
> Demo here: http://23.24.168.98:3003/ 
> 
> To view the demo: point metamask to our private network in the network settings: http://23.24.168.98:8545
> 
> Install Metamask here: https://metamask.io/

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
# requires the server running if standalone
# don't forget to change the host endpoint in ./src/api/relayerClient.js to point to the server! (or leave alone if building for prod)
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

## Server Build Setup
Requires Docker: https://www.docker.com/community-edition#/download
``` bash
# navigate to server folder
cd ./server

# starts http server at localhost:3003
# starts websocket server at localhost:3004
# built client side files are included; no need to build client using the above instructions to run the app if you aren't making changes
docker-compose up -d
```
