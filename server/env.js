const env = {
  host: '0.0.0.0',
  port: 3003,
  ws: 3004
}

module.exports = env
