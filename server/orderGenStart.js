const { ZeroEx } = require('0x.js')
const { HttpClient } = require('@0xproject/connect')
const { BigNumber } = require('@0xproject/utils')
const Web3 = require('web3')

const mainAsync = async () => {
  const provider = new Web3.providers.HttpProvider('http://23.24.168.98:8545')
  const configs = {
    networkId: 567820,
    zrxContractAddress: '0x95144990c0fc35f9b7ec90b1eb1b3bb892d393c3',
    exchangeContractAddress: '0xa9f25264de8c83b577641249e63f1e4116b49ad4',
    tokenRegistryContractAddress: '0xba47ed9dc6362e39a579e3678de98e615270fa37',
    tokenTransferProxyContractAddress: '0x7fd2b7e0ea412a6ea59349322529b049c8ddebec'
  }
  const zeroEx = new ZeroEx(provider, configs)
  const web3 = new Web3(provider)
  const relayerClient = new HttpClient('http://23.24.168.98:3003/v0')

  // Get exchange contract address
  const EXCHANGE_ADDRESS = await zeroEx.exchange.getContractAddress()

  // Get token information
  const wethTokenInfo = await zeroEx.tokenRegistry.getTokenBySymbolIfExistsAsync('WETH')
  const zrxTokenInfo = await zeroEx.tokenRegistry.getTokenBySymbolIfExistsAsync('ZRX')

  // Check if either getTokenBySymbolIfExistsAsync query resulted in undefined
  if (wethTokenInfo === undefined || zrxTokenInfo === undefined) {
    throw new Error('could not find token info')
  }

  // Get token contract addresses
  const WETH_ADDRESS = wethTokenInfo.address
  const ZRX_ADDRESS = zrxTokenInfo.address

  // Get all available addresses
  const addresses = (await zeroEx.getAvailableAddressesAsync()).slice(0, 4)

  // Get the first address, this address is preloaded with a ZRX balance from the snapshot
  // const zrxOwnerAddress = addresses[0]

  // Assign other addresses as WETH owners
  const wethOwnerAddresses = addresses.slice(1)

  // unlock all accounts
  addresses.forEach(address => {
    web3.personal.unlockAccount(address, '')
  })

  console.log('accounts unlocked')

  // Set WETH and ZRX unlimited allowances for all addresses
  // const setZrxAllowanceTxHashes = await Promise.all(
  //   addresses.map(address => {
  //     return zeroEx.token.setUnlimitedProxyAllowanceAsync(ZRX_ADDRESS, address)
  //   })
  // )
  // const setWethAllowanceTxHashes = await Promise.all(
  //   addresses.map(address => {
  //     return zeroEx.token.setUnlimitedProxyAllowanceAsync(WETH_ADDRESS, address)
  //   })
  // )
  // await Promise.all(
  //   setZrxAllowanceTxHashes.concat(setWethAllowanceTxHashes).map(tx => {
  //     return zeroEx.awaitTransactionMinedAsync(tx)
  //   })
  // )

  console.log('allowances approved')

  // Send 1000 ZRX from zrxOwner to all other addresses
  // await Promise.all(
  //   wethOwnerAddresses.map(async (address, index) => {
  //     const zrxToTransfer = ZeroEx.toBaseUnitAmount(new BigNumber(1000), zrxTokenInfo.decimals)
  //     const txHash = await zeroEx.token.transferAsync(
  //       ZRX_ADDRESS,
  //       zrxOwnerAddress,
  //       address,
  //       zrxToTransfer
  //     )
  //     return zeroEx.awaitTransactionMinedAsync(txHash)
  //   })
  // )

  console.log('zrx distributed')

  // Deposit ETH and generate WETH tokens for each address in wethOwnerAddresses
  // const ethToConvert = ZeroEx.toBaseUnitAmount(new BigNumber(20), wethTokenInfo.decimals)
  // const depositTxHashes = await Promise.all(
  //   addresses.map(address => {
  //     return zeroEx.etherToken.depositAsync(WETH_ADDRESS, ethToConvert, address)
  //   })
  // )
  // await Promise.all(
  //   depositTxHashes.map(tx => {
  //     return zeroEx.awaitTransactionMinedAsync(tx)
  //   })
  // )

  console.log('weth converted')

  // Generate and submit orders with increasing ZRX/WETH exchange rate
  await Promise.all(
    wethOwnerAddresses.map(async (address, index) => {
      // Programmatically determine the exchange rate based on the index of address in wethOwnerAddresses
      const exchangeRate = (index + 1) // ZRX/WETH
      const makerTokenAmount = ZeroEx.toBaseUnitAmount(new BigNumber(1), wethTokenInfo.decimals)
      const takerTokenAmount = makerTokenAmount.mul(exchangeRate)

      // Generate fees request for the order
      const ONE_WEEK_IN_MS = 3600000 * 24 * ~~(Math.random() * 7 + 1)
      const feesRequest = {
        exchangeContractAddress: EXCHANGE_ADDRESS,
        maker: address,
        taker: ZeroEx.NULL_ADDRESS,
        makerTokenAddress: WETH_ADDRESS,
        takerTokenAddress: ZRX_ADDRESS,
        makerTokenAmount,
        takerTokenAmount,
        expirationUnixTimestampSec: new BigNumber(Date.now() + ONE_WEEK_IN_MS),
        salt: ZeroEx.generatePseudoRandomSalt()
      }

      // Send fees request to relayer and receive a FeesResponse instance
      const feesResponse = await relayerClient.getFeesAsync(feesRequest)

      // Combine the fees request and response to from a complete order
      const order = {
        ...feesRequest,
        ...feesResponse
      }

      // Create orderHash
      const orderHash = ZeroEx.getOrderHashHex(order)

      // Sign orderHash and produce a ecSignature
      const ecSignature = await zeroEx.signOrderHashAsync(orderHash, address, false)

      // Append signature to order
      const signedOrder = {
        ...order,
        ecSignature
      }

      // Submit order to relayer
      await relayerClient.submitOrderAsync(signedOrder)
    })
  )

  console.log('ZRX/WETH exchange rate')

  // Generate and submit orders with flat WETH/ZRX exchange rate
  await Promise.all(
    wethOwnerAddresses.map(async (address, index) => {
      const makerTokenAmount = ZeroEx.toBaseUnitAmount(new BigNumber(1), wethTokenInfo.decimals)
      const takerTokenAmount = ZeroEx.toBaseUnitAmount(new BigNumber(0.5), wethTokenInfo.decimals)

      // Generate fees request for the order
      const ONE_WEEK_IN_MS = 3600000 * 24 * ~~(Math.random() * 7 + 1)
      const feesRequest = {
        exchangeContractAddress: EXCHANGE_ADDRESS,
        maker: address,
        taker: ZeroEx.NULL_ADDRESS,
        makerTokenAddress: ZRX_ADDRESS,
        takerTokenAddress: WETH_ADDRESS,
        makerTokenAmount,
        takerTokenAmount,
        expirationUnixTimestampSec: new BigNumber(Date.now() + ONE_WEEK_IN_MS),
        salt: ZeroEx.generatePseudoRandomSalt()
      }

      // Send fees request to relayer and receive a FeesResponse instance
      const feesResponse = await relayerClient.getFeesAsync(feesRequest)

      // Combine the fees request and response to from a complete order
      const order = {
        ...feesRequest,
        ...feesResponse
      }

      // Create orderHash
      const orderHash = ZeroEx.getOrderHashHex(order)

      // Sign orderHash and produce a ecSignature
      const ecSignature = await zeroEx.signOrderHashAsync(orderHash, address, false)

      // Append signature to order
      const signedOrder = {
        ...order,
        ecSignature
      }

      // Submit order to relayer
      await relayerClient.submitOrderAsync(signedOrder)
    })
  )
  console.log('WETH/ZRX exchange rate')
}

mainAsync().catch(console.error)
