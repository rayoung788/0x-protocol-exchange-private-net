var { ZeroEx } = require('0x.js')
const Web3 = require('web3')
var utils = require('@0xproject/utils')
var bodyParser = require('body-parser')
var express = require('express')
var http = require('http')
var websocket = require('websocket')
var cors = require('cors')
const path = require('path')
const env = require('./env')
// Global state for holding orders: ID -> Order
// TODO: Needs to be moved to a database for persistence
var orders = {}
// unique ID for orders (theres probably a better way to do this)
var idx = 0
var socketConnection
// HTTP Server
var app = express()
// sets the cors headers for all clients **Remove in production environments**
app.use(cors())
app.use(express.static('static'))
app.use(bodyParser.json())
// Hard-coded the provider & config to point to our private testnet
// TODO: make this configurable
const provider = new Web3.providers.HttpProvider('http://23.24.168.98:8545')
const configs = {
  networkId: 567820,
  zrxContractAddress: '0x95144990c0fc35f9b7ec90b1eb1b3bb892d393c3',
  exchangeContractAddress: '0xa9f25264de8c83b577641249e63f1e4116b49ad4',
  tokenRegistryContractAddress: '0xba47ed9dc6362e39a579e3678de98e615270fa37',
  tokenTransferProxyContractAddress: '0x7fd2b7e0ea412a6ea59349322529b049c8ddebec'
}
const zeroEx = new ZeroEx(provider, configs)
// Checks for invalid orders every 2 seconds
// TODO: use more sophisticated order validation referenced here: https://0xproject.com/wiki#0x-OrderWatcher
setInterval(() => {
  Object.values(orders).forEach(async order => {
    try {
      const prospect = { ...order }
      // ZeroEx expects amounts and time to be big nuimbers
      prospect.makerTokenAmount = new utils.BigNumber(prospect.makerTokenAmount)
      prospect.takerTokenAmount = new utils.BigNumber(prospect.takerTokenAmount)
      prospect.expirationUnixTimestampSec = new utils.BigNumber(prospect.expirationUnixTimestampSec)
      await zeroEx.exchange.validateOrderFillableOrThrowAsync(prospect)
    } catch (err) {
      const reason = err.message.split('.')[0]
      // Ignore invalid JSON RPC responses
      // TODO: determine why invalid JSON RPC responses occur
      if (!reason.includes('Invalid JSON RPC response')) {
        // delete the invalid order and update all listening clients
        const idx = order.idx
        order.reason = reason
        console.log('invalid', idx, reason)
        delete orders[idx]
        if (socketConnection !== undefined) {
          var message = {
            type: 'prune',
            channel: 'orderbook',
            requestId: 1,
            payload: order
          }
          socketConnection.send(JSON.stringify(message))
        }
      }
    }
  })
}, 2000)
// route to serve the client-side code
app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, '/index.html'))
})
// route to request an orderbook for a BASE/QUOTE token pair
app.get('/v0/orderbook', (req, res) => {
  console.log('HTTP: GET orderbook')
  var baseTokenAddress = req.param('baseTokenAddress')
  var quoteTokenAddress = req.param('quoteTokenAddress')
  res.status(201).send(renderOrderBook(baseTokenAddress, quoteTokenAddress))
})
// route to submit a new order
app.post('/v0/order', (req, res) => {
  console.log('HTTP: POST order')
  var order = req.body
  order.idx = idx
  // increment the order ID to maintain uniqueness
  idx += 1
  orders[order.idx] = order
  if (socketConnection !== undefined) {
    // send new order update to all listening clients
    var message = {
      type: 'update',
      channel: 'orderbook',
      requestId: 1,
      payload: order
    }
    socketConnection.send(JSON.stringify(message))
  }
  res.status(201).send({})
})
// route to get fees used for new orders
// currently set to zero fees
app.post('/v0/fees', (req, res) => {
  console.log('HTTP: POST fees')
  var makerFee = new utils.BigNumber(0).toString()
  var takerFee = new utils.BigNumber(0).toString() // ZeroEx.toBaseUnitAmount(new utils.BigNumber(10), 18).toString()
  res.status(201).send({
    // TODO: add recipient to accept fees
    feeRecipient: ZeroEx.NULL_ADDRESS,
    makerFee: makerFee,
    takerFee: takerFee
  })
})
app.listen(env.port, env.host, () => {
  return console.log(`Standard relayer API (HTTP) listening on port ${env.port}!`)
})
// WebSocket server
var server = http.createServer((request, response) => {
  console.log(new Date() + ' Received request for ' + request.url)
  response.writeHead(404)
  response.end()
})
server.listen(env.ws, env.host, () => {
  console.log(`Standard relayer API (WS) listening on port ${env.ws}!`)
})
var wsServer = new websocket.server({
  httpServer: server,
  autoAcceptConnections: false
})
wsServer.on('request', request => {
  socketConnection = request.accept()
  console.log('WS: Connection accepted')
  socketConnection.on('message', message => {
    if (message.type === 'utf8' && message.utf8Data !== undefined) {
      var parsedMessage = JSON.parse(message.utf8Data)
      console.log('WS: Received Message: ' + parsedMessage.type)
      var snapshotNeeded = parsedMessage.payload.snapshot
      var baseTokenAddress = parsedMessage.payload.baseTokenAddress
      var quoteTokenAddress = parsedMessage.payload.quoteTokenAddress
      var requestId = parsedMessage.requestId
      if (snapshotNeeded && socketConnection !== undefined) {
        var orderbook = renderOrderBook(baseTokenAddress, quoteTokenAddress)
        var returnMessage = {
          type: 'snapshot',
          channel: 'orderbook',
          requestId: requestId,
          payload: orderbook
        }
        socketConnection.sendUTF(JSON.stringify(returnMessage))
      }
    }
  })
  socketConnection.on('close', (reasonCode, description) => {
    console.log('WS: Peer disconnected')
  })
})
// returns an order book for the specified BASE/QUOTE pair
function renderOrderBook(baseTokenAddress, quoteTokenAddress) {
  var bids = Object.values(orders).filter(order => {
    return (
      order.takerTokenAddress === baseTokenAddress && order.makerTokenAddress === quoteTokenAddress
    )
  })
  var asks = Object.values(orders).filter(order => {
    return (
      order.takerTokenAddress === quoteTokenAddress && order.makerTokenAddress === baseTokenAddress
    )
  })
  return {
    bids: bids.reduce((res, bid) => {
      res[bid.idx] = bid
      return res
    }, {}),
    asks: asks.reduce((res, ask) => {
      res[ask.idx] = ask
      return res
    }, {})
  }
}
